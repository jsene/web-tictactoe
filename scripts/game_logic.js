//Global variables
let madeMove = false;

//Draw an X in the box selected by the user
function draw(id, player) {
  let box = document.querySelector('#' + id);
  box.textContent = player;

  //Add player (X || O) to classlist when box is filled
  box.classList.toggle(player);
}

function reset(haveWinner, player) {
  if (haveWinner) {
    if (window.confirm(player + " Wins!") == true) {
      let boxes = document.querySelectorAll('.box');
      boxes.forEach((box) => {
        box.textContent = "";
        box.classList.remove('X');
        box.classList.remove('O');
      })
    }
  }
}

function checkDraw() {
  //Check for Draw
  let locations = ['ul', 'um', 'ur', 'ml', 'mm', 'mr', 'll', 'lm', 'lr'];
  sum = 0;
  for (let i=0; i<locations.length; i++) {
    let box = document.querySelector('#' + locations[i]);
    if (box.classList.contains('X') || box.classList.contains('O')) {
      sum += 1;
    }
  }

  if (sum == 9) {
    if (window.confirm("Draw!") == true) {
      let boxes = document.querySelectorAll('.box');
      boxes.forEach((box) => {
        box.textContent = "";
        box.classList.remove('X');
        box.classList.remove('O');
      })
    }
  }
}

//Check for game end
function checkForWin() {
  let haveWinner = false;

  haveWinner = checkHorizontal('O');
  reset(haveWinner, 'O');

  haveWinner = checkVertical('O');
  reset(haveWinner, 'O');

  haveWinner = checkDiagonal('O');
  reset(haveWinner, 'O');

  haveWinner = checkHorizontal('X');
  reset(haveWinner, 'X');

  haveWinner = checkVertical('X');
  reset(haveWinner, 'X');

  haveWinner = checkDiagonal('X');
  reset(haveWinner, 'X');
}

//If computer can win play that spot, otherwise block X if it is about to win
function checkImpendingWin() {
  checkHorizontal('O', 'O');
  checkVertical('O', 'O');
  checkDiagonal('O', 'O');
  checkHorizontal('X', 'O');
  checkVertical('X', 'O');
  checkDiagonal('X', 'O');
}

function randomMove() {
  let locations = ['ul', 'um', 'ur', 'ml', 'mm', 'mr', 'll', 'lm', 'lr'];
  let foundLocation = false;

  if (!madeMove) {
    while (!foundLocation) {
      let index = Math.floor(Math.random() * (locations.length));
      let box = document.querySelector('#' + locations[index]);
      if (!box.classList.contains('X') && !box.classList.contains('O')) {
        draw(locations[index], 'O');
        foundLocation = true;
        madeMove = true;
      }
    }
  }
}

function checkHorizontal(mark, player) {
  let upperRow = ['ul', 'um', 'ur'];
  let midRow = ['ml', 'mm', 'mr'];
  let bottomRow = ['ll', 'lm', 'lr'];

  upperSum = 0;
  middleSum = 0;
  lowerSum = 0;
  for (let i = 0; i < 3; i++) {
    let box = document.querySelector('#' + upperRow[i]);
    if (box.classList.contains(mark)) {
      upperSum += 1;
    }

    box = document.querySelector('#' + midRow[i]);
    if (box.classList.contains(mark)) {
      middleSum += 1;
    }

    box = document.querySelector('#' + bottomRow[i]);
    if (box.classList.contains(mark)) {
      lowerSum += 1;
    }
  }

  //Check if game is finished
  if (lowerSum == 3 || middleSum == 3 || upperSum == 3) {
    //Game is finished
    return true;
  }

  //Block impending win
  if (!madeMove) {
    if (upperSum >= 2) {
      for (let i = 0; i < 3; i++) {
        box = document.querySelector('#' + upperRow[i]);
        if (!box.classList.contains('X') && !box.classList.contains('O')) {
          draw(upperRow[i], player);
          madeMove = true;
        }
      }
    }
  }

  if (!madeMove) {
    if (middleSum >= 2) {
      for (let i = 0; i < 3; i++) {
        box = document.querySelector('#' + midRow[i]);
        if (!box.classList.contains('X') && !box.classList.contains('O')) {
          draw(midRow[i], player);
          madeMove = true;
        }
      }
    }
  }

  if (!madeMove) {
    if (lowerSum >= 2) {
      for (let i = 0; i < 3; i++) {
        box = document.querySelector('#' + bottomRow[i]);
        if (!box.classList.contains('X') && !box.classList.contains('O')) {
          draw(bottomRow[i], player);
          madeMove = true;
        }
      }
    }
  }
}

function checkVertical(mark, player) {
  let leftCol = ['ul', 'ml', 'll'];
  let midCol = ['um', 'mm', 'lm'];
  let rightCol = ['ur', 'mr', 'lr'];

  leftSum = 0;
  middleSum = 0;
  rightSum = 0;
  for (let i = 0; i < 3; i++) {
    let box = document.querySelector('#' + leftCol[i]);
    if (box.classList.contains(mark)) {
      leftSum += 1;
    }

    box = document.querySelector('#' + midCol[i]);
    if (box.classList.contains(mark)) {
      middleSum += 1;
    }

    box = document.querySelector('#' + rightCol[i]);
    if (box.classList.contains(mark)) {
      rightSum += 1;
    }
  }

  //Check if game is finished
  if (leftSum == 3 || middleSum == 3 || rightSum == 3) {
    //Game is finished
    return true;
  }

  if (!madeMove) {
    //Block impending win
    if (leftSum >= 2) {
      for (let i = 0; i < 3; i++) {
        box = document.querySelector('#' + leftCol[i]);
        if (!box.classList.contains('X') && !box.classList.contains('O')) {
          draw(leftCol[i], player);
          madeMove = true;
        }
      }
    }
  }

  if (!madeMove) {
    if (middleSum >= 2) {
      for (let i = 0; i < 3; i++) {
        box = document.querySelector('#' + midCol[i]);
        if (!box.classList.contains('X') && !box.classList.contains('O')) {
          draw(midCol[i], player);
          madeMove = true;
        }
      }
    }
  }

  if (!madeMove) {
    if (rightSum >= 2) {
      for (let i = 0; i < 3; i++) {
        box = document.querySelector('#' + rightCol[i]);
        if (!box.classList.contains('X') && !box.classList.contains('O')) {
          draw(rightCol[i], player);
          madeMove = true;
        }
      }
    }
  }
}

function checkDiagonal(mark, player) {
  leftDiag = ['ul', 'mm', 'lr'];
  rightDiag = ['ur', 'mm', 'll'];

  leftSum = 0;
  rightSum = 0;
  for (let i=0; i<3; i++) {
    let box = document.querySelector('#' + leftDiag[i]);
    if (box.classList.contains(mark)) {
      leftSum += 1;
    }

    box = document.querySelector('#' + rightDiag[i]);
    if (box.classList.contains(mark)) {
      rightSum += 1;
    }
  }

  //Check if game is finished
  if (leftSum == 3 || rightSum == 3) {
    //Game is finished
    return true;
  }

  if (!madeMove) {
    if (rightSum >= 2) {
      for (let i = 0; i < 3; i++) {
        box = document.querySelector('#' + rightDiag[i]);
        if (!box.classList.contains('X') && !box.classList.contains('O')) {
          draw(rightDiag[i], player);
          madeMove = true;
        }
      }
    }
  }

  if (!madeMove) {
    if (leftSum >= 2) {
      for (let i = 0; i < 3; i++) {
        box = document.querySelector('#' + leftDiag[i]);
        if (!box.classList.contains('X') && !box.classList.contains('O')) {
          draw(leftDiag[i], player);
          madeMove = true;
        }
      }
    }
  }
}

// Add event listener for when a box is clicked by user
const buttons = document.querySelectorAll('.box');
buttons.forEach((button) => {
  button.addEventListener('click', function(e){
    let id = e.target.id;
    let box = document.querySelector('#' + id);
    if (!box.classList.contains('X') && !box.classList.contains('O')) {
      draw(id, 'X');
      checkDraw();
      checkForWin();
      madeMove = false;
      checkImpendingWin();
      randomMove();
      checkDraw();
      checkForWin();
      madeMove = false;
    } else {
      alert("This location has already been played!");
    }
  })
})
